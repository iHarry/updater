﻿namespace Updater
{
    public enum UpdaterState
    {
        Default,
        Start,
        CheckingFiles,
        Cancel
    }
}
