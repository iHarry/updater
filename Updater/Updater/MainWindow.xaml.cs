﻿using UpdateEngine;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using Updater.Properties;

namespace Updater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeView();
        }

        private void InitializeView()
        {
            UpdateWindowState(UpdaterState.Default);
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            UpdateWindowState(UpdaterState.Start);
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Settings.Default.AppPath, Settings.Default.AppFileName);
            Application app = Application.Current;
            try
            {
                process.Start();
                app.Shutdown();
            }
            catch (Exception)
            {
                txtInfoPanel.Content = "Application is not found.";
                UpdateWindowState(UpdaterState.Default);
            }
        }

        private async void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            UpdateWindowState(UpdaterState.CheckingFiles);

            UpdaterManager.Instance.CheckingProgressChanged += UpdaterManager_CheckingProgressChanged;
            UpdaterManager.Instance.DownloadStarted += UpdaterManager_DownloadStarted;
            UpdaterManager.Instance.DownloadProgressChanged += UpdaterManager_DownloadProgressChanged;
            UpdaterManager.Instance.OperationFailed += UpdaterManager_OperationFailed;
            UpdaterManager.Instance.ProcessEnded += UpdaterManager_ProcessEnded;

            await UpdaterManager.Instance.UpdateFilesAsync();
        }

        private void UpdaterManager_CheckingProgressChanged(object sender, CheckingProgressChangedEventArgs args)
        {
            pbTotal.Value = 0;
            pbFile.Value = 0;
            txtInfoPanel.Content = String.Format("Checking: {0}", args.Name);
        }

        private void UpdaterManager_DownloadStarted(object sender, DownloadStartedEventArgs e)
        {
            pbTotal.Maximum = e.FilesSize;
            pbTotal.Value = 0;
        }

        private void UpdaterManager_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs args)
        {
            pbFile.Maximum = args.Size;
            pbFile.Value = args.DownloadedSize;

            if(pbFile.Maximum == pbFile.Value)
            {
                pbTotal.Value += pbFile.Value;
            }

            double totalDownloadedSizeMb = pbTotal.Maximum / 1024 / 1024;
            double downloadedSizePercent = (double)pbTotal.Value / pbTotal.Maximum * 100;
            txtInfoPanel.Content = String.Format("Downloading: {0} (Total: {1:0.00}MB {2: 0.0} %)", args.Name, totalDownloadedSizeMb, downloadedSizePercent);
        }

        private void UpdaterManager_OperationFailed(object sender, OperationFailedEventArgs args)
        {
            if (args.Reason == FailReason.DownloadError)
            {
                txtInfoPanel.Content = "Downloading file error. Please, try again.";
            }
            else if (args.Reason == FailReason.Error)
            {
                txtInfoPanel.Content = "Something went wrong. Please, try again.";
            }
            else if (args.Reason == FailReason.IOError)
            {
                txtInfoPanel.Content = "IO error.";
            }
            else if (args.Reason == FailReason.NotEnoughFreeSpace)
            {
                txtInfoPanel.Content = "Not enough free space.";
            }
            else if (args.Reason == FailReason.ServerError)
            {
                txtInfoPanel.Content = "Update server connection error.";
            }
            else if (args.Reason == FailReason.UserCancel)
            {
                txtInfoPanel.Content = "Update is canceled by user.";
            }

            UpdateWindowState(UpdaterState.Default);
        }

        private void UpdaterManager_ProcessEnded(object sender, EventArgs e)
        {
            txtInfoPanel.Content = "Update files completed.";
            UpdateWindowState(UpdaterState.Default);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            UpdateWindowState(UpdaterState.Cancel);
            txtInfoPanel.Content = "Canceling in process...";

            UpdaterManager.Instance.Cancel();
        }

        private void UpdateWindowState(UpdaterState state)
        {
            switch (state)
            {
                case UpdaterState.Start:
                    btnStart.IsEnabled = false;
                    btnCheck.IsEnabled = false;
                    btnCancel.IsEnabled = false;
                    break;
                case UpdaterState.CheckingFiles:
                    btnStart.IsEnabled = false;
                    btnCheck.IsEnabled = false;
                    btnCancel.IsEnabled = true;
                    break;
                case UpdaterState.Cancel:
                    btnStart.IsEnabled = false;
                    btnCheck.IsEnabled = false;
                    btnCancel.IsEnabled = false;
                    break;
                default:
                    pbFile.Value = 0;
                    pbTotal.Value = 0;
                    btnStart.IsEnabled = true;
                    btnCheck.IsEnabled = true;
                    btnCancel.IsEnabled = false;
                    break;
            }

        }
    }
}
