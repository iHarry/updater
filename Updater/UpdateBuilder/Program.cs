﻿using UpdateEngine;
using System;
using System.Threading.Tasks;

namespace UpdateBuilder
{
    class Program
    {
        static int Main(string[] args)
        {
            ResultCode resultCode = StartBuildUpdateFileAsync().Result;

            return (int)resultCode;
        }

        private static async Task<ResultCode> StartBuildUpdateFileAsync()
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelHandler);
            UpdaterManager.Instance.CheckingProgressChanged += UpdaterManager_CheckingProgressChanged;
            UpdaterManager.Instance.ProcessEnded += UpdaterManager_ProcessEnded;
            UpdaterManager.Instance.OperationFailed += UpdaterManager_OperationFailed;
            ResultCode resultCode = await UpdaterManager.Instance.BuildUpdateFileAsync();

            return resultCode;
        }

        private static void CancelHandler(object sender, ConsoleCancelEventArgs e)
        {
            UpdaterManager.Instance.Cancel();
        }

        private static void UpdaterManager_CheckingProgressChanged(object sender, CheckingProgressChangedEventArgs args)
        {
            Console.WriteLine(args.Name);
        }

        private static void UpdaterManager_ProcessEnded(object sender, EventArgs e)
        {
            Console.WriteLine();
            Console.WriteLine("Build update file completed.");
        }

        private static void UpdaterManager_OperationFailed(object sender, OperationFailedEventArgs args)
        {
            if (args.Reason == FailReason.Error)
            {
                Console.WriteLine("Something went wrong. Please, try again.");
            }
            else if (args.Reason == FailReason.IOError)
            {
                Console.WriteLine("IO error.");
            }
            else if (args.Reason == FailReason.UserCancel)
            {
                Console.WriteLine("Build update is canceled by user.");
            }
        }
    }
}
