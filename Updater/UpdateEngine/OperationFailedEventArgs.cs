﻿using System;

namespace UpdateEngine
{
    public delegate void OperationFailedEventHandler(object sender, OperationFailedEventArgs args);

    public class OperationFailedEventArgs : EventArgs
    {
        public FailReason Reason { get; protected set; }

        public OperationFailedEventArgs(FailReason reason)
        {
            Reason = reason;
        }
    }
}
