﻿namespace UpdateEngine
{
    internal class FileDetail
    {
        public string Path { get; set; }
        public string Hash { get; set; }
        public long Size { get; set; }
    }
}
