﻿using System;

namespace UpdateEngine
{
    public delegate void DownloadStartedEventHandler(object sender, DownloadStartedEventArgs args);

    public class DownloadStartedEventArgs : EventArgs
    {
        public int FilesCount { get; set; }
        public long FilesSize { get; set; }
    }
}
