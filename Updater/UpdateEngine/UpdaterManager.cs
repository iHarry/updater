﻿using UpdateEngine.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace UpdateEngine
{
    public class UpdaterManager
    {
        public event CheckingProgressChangedEventHandler CheckingProgressChanged;
        public event DownloadStartedEventHandler DownloadStarted;
        public event DownloadProgressChangedEventHandler DownloadProgressChanged;
        public event OperationFailedEventHandler OperationFailed;
        public event EventHandler ProcessEnded;

        internal static string ServerUrl
        {
            get
            {
                return Settings.Default.ServerUrl;
            }
        }

        internal static string UpdateFileName
        {
            get
            {
                return Settings.Default.UpdateFileName;
            }
        }

        internal static string SourcePath
        {
            get
            {
                return Settings.Default.SourcePath;
            }
        }

        internal static IEnumerable<string> FileExceptions
        {
            get
            {
                return Settings.Default.FileExceptions.Cast<string>();
            }
        }

        private CancellationTokenSource _cts;

        private static UpdaterManager _instance;

        private UpdaterManager() { }

        public async Task<ResultCode> UpdateFilesAsync()
        {
            _cts = new CancellationTokenSource();
            string sourcePath = GetBaseDirectoryPath();
            var hashWorker = new HashWorker();
            var webWorker = new WebWorker();

            hashWorker.CheckingProgressChanged += HashWorker_CheckingProgressChanged;
            webWorker.DownloadProgressChanged += WebWorker_DownloadProgressChanged;

            try
            {
                await webWorker.DownloadFileAsync(new FileDetail { Path = UpdateFileName }, _cts.Token);

                List<FileDetail> localFileDetails = await hashWorker.ComputeFromDirectoryAsync(_cts.Token, sourcePath);
                List<FileDetail> serverFileDetails = hashWorker.LoadFromFile(sourcePath, UpdateFileName);
                List<FileDetail> downloadList = GetDownloadList(serverFileDetails, localFileDetails);

                foreach (FileDetail checkFileInfo in downloadList)
                {
                    if (_cts.Token.IsCancellationRequested)
                    {
                        new OperationCanceledException();
                    }
                    else
                    {
                        await webWorker.DownloadFileAsync(checkFileInfo, _cts.Token);
                    }
                }
                OnProcessEnded(EventArgs.Empty);
            }
            catch (OperationCanceledException)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.UserCancel));
                return ResultCode.Error;
            }
            catch (IOException ex)
            {
                foreach (ResultCode key in ex.Data.Keys)
                {
                    if (key == ResultCode.RequiredFreeSpace)
                    {
                        OnOperationFailed(new OperationFailedEventArgs(FailReason.NotEnoughFreeSpace));
                        return ResultCode.RequiredFreeSpace;
                    }
                    else
                    {
                        OnOperationFailed(new OperationFailedEventArgs(FailReason.IOError));
                        return ResultCode.Error;
                    }
                }
            }
            catch (DownloadException)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.DownloadError));
                return ResultCode.Error;
            }
            catch (WebException)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.ServerError));
                return ResultCode.Error;
            }
            catch (Exception)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.Error));
                return ResultCode.Error;
            }

            return ResultCode.Success;
        }

        public async Task<ResultCode> BuildUpdateFileAsync()
        {
            _cts = new CancellationTokenSource();

            var hashWorker = new HashWorker();

            hashWorker.CheckingProgressChanged += HashWorker_CheckingProgressChanged;

            string sourcePath = SourcePath;
            if (sourcePath == string.Empty)
            {
                sourcePath = GetBaseDirectoryPath();
            }
            string updateFilePath = Path.Combine(sourcePath, UpdateFileName);

            try
            {
                List<FileDetail> fileDetails = await hashWorker.ComputeFromDirectoryAsync(_cts.Token, sourcePath);

                await hashWorker.WriteToFileAsync(fileDetails, sourcePath, UpdateFileName);

                OnProcessEnded(EventArgs.Empty);
            }
            catch (OperationCanceledException)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.UserCancel));
                return ResultCode.Error;
            }
            catch (IOException)
            {
                if (File.Exists(updateFilePath))
                {
                    File.Delete(updateFilePath);
                }
                OnOperationFailed(new OperationFailedEventArgs(FailReason.IOError));
                return ResultCode.Error;
            }
            catch (Exception)
            {
                OnOperationFailed(new OperationFailedEventArgs(FailReason.Error));
                return ResultCode.Error;
            }

            return ResultCode.Success;
        }

        public void Cancel()
        {
            _cts.Cancel();
            _cts.Dispose();
        }

        internal static string GetBaseDirectoryPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        internal static string GetRelativeDirectoryPath(string fullDirectoryPath, string sourcePath)
        {
            return fullDirectoryPath.Replace(sourcePath, String.Empty).TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }

        private void OnCheckingProgressChanged(CheckingProgressChangedEventArgs args)
        {
            if (CheckingProgressChanged != null)
                CheckingProgressChanged(null, args);
        }

        protected virtual void OnDownloadStarted(DownloadStartedEventArgs args)
        {
            if (DownloadStarted != null)
                DownloadStarted(this, args);
        }

        protected virtual void OnDownloadProgressChanged(DownloadProgressChangedEventArgs args)
        {
            if (DownloadProgressChanged != null)
                DownloadProgressChanged(this, args);
        }

        protected virtual void OnOperationFailed(OperationFailedEventArgs args)
        {
            if (OperationFailed != null)
                OperationFailed(this, args);
        }

        protected virtual void OnProcessEnded(EventArgs args)
        {
            if (ProcessEnded != null)
                ProcessEnded(this, args);
        }

        private void HashWorker_CheckingProgressChanged(object sender, CheckingProgressChangedEventArgs args)
        {
            OnCheckingProgressChanged(args);
        }

        private void WebWorker_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs args)
        {
            OnDownloadProgressChanged(args);
        }

        private List<FileDetail> GetDownloadList(List<FileDetail> serverFileDetails, List<FileDetail> localFileDetails)
        {
            var downloadList = new List<FileDetail>();
            long filesSize = 0;
            foreach (FileDetail serverFileDetail in serverFileDetails)
            {
                if (_cts.Token.IsCancellationRequested)
                {
                    throw new OperationCanceledException();
                }
                else
                {
                    if (localFileDetails.Any(f => f.Hash == serverFileDetail.Hash && f.Size == serverFileDetail.Size))
                    {
                        continue;
                    }

                    filesSize += serverFileDetail.Size;
                    downloadList.Add(serverFileDetail);
                }
            }

            DriveInfo di = new DriveInfo(GetBaseDirectoryPath());
            long availableFreeSpace = di.AvailableFreeSpace;
            if (availableFreeSpace < filesSize)
            {
                var ex = new IOException("Not enough free space");
                ex.Data.Add(ResultCode.RequiredFreeSpace, filesSize - availableFreeSpace);
                throw ex;
            }

            OnDownloadStarted(new DownloadStartedEventArgs { FilesCount = downloadList.Count, FilesSize = filesSize });

            return downloadList;
        }

        public static UpdaterManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UpdaterManager();
                }
                return _instance;
            }
        }
    }
}
