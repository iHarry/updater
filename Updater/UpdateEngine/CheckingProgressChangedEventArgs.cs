﻿using System;

namespace UpdateEngine
{
    public delegate void CheckingProgressChangedEventHandler(object sender, CheckingProgressChangedEventArgs args);

    public class CheckingProgressChangedEventArgs : EventArgs
    {
        public string Name { get; set; }
    }
}
