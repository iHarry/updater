﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace UpdateEngine
{
    internal class WebWorker
    {
        public event DownloadProgressChangedEventHandler DownloadProgressChanged;

        private const int BufferSize = 4096;
        private const int ResponseTimeOut = 5000;

        public async Task DownloadFileAsync(FileDetail fileDetail, CancellationToken ct)
        {
            string serverFilePath = Path.Combine(UpdaterManager.ServerUrl, fileDetail.Path);
            string clientFilePath = Path.Combine(UpdaterManager.GetBaseDirectoryPath(), fileDetail.Path);
            string fileName = Path.GetFileName(fileDetail.Path);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serverFilePath);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                long downloadedSize = 0;
                long fileSize = response.ContentLength;

                Directory.CreateDirectory(Path.GetDirectoryName(clientFilePath));

                using (Stream responseStream = response.GetResponseStream())
                {
                    responseStream.ReadTimeout = ResponseTimeOut;

                    using (FileStream fileStream = new FileStream(clientFilePath, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize: BufferSize))
                    {
                        byte[] buffer = new byte[BufferSize];
                        int bytesRead = 0;
                        while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            if (ct.IsCancellationRequested)
                            {
                                throw new OperationCanceledException();
                            }
                            else
                            {
                                await fileStream.WriteAsync(buffer, 0, bytesRead);

                                downloadedSize += bytesRead;

                                OnDownloadProgressChanged(new DownloadProgressChangedEventArgs { Name = fileName, Size = fileSize, DownloadedSize = downloadedSize });
                            }
                        }
                    }
                }

                // Verify file
                string hash = string.Empty;
                using (var stream = File.OpenRead(clientFilePath))
                {
                    hash = HashUtility.GetHashChecksum(stream);
                }

                if (fileDetail.Path == UpdaterManager.UpdateFileName)
                {
                    if (fileSize != downloadedSize)
                    {
                        throw new DownloadException(string.Format("Verify {0} error.", fileDetail.Path));
                    }
                }
                else if (fileDetail.Size != downloadedSize && fileDetail.Hash != hash)
                {
                    throw new DownloadException(string.Format("Verify {0} error.", fileDetail.Path));
                }
            }
        }

        protected virtual void OnDownloadProgressChanged(DownloadProgressChangedEventArgs args)
        {
            if (DownloadProgressChanged != null)
                DownloadProgressChanged(this, args);
        }
    }
}
