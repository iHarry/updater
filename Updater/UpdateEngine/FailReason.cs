﻿namespace UpdateEngine
{
    public enum FailReason
    {
        DownloadError,
        Error,
        IOError,
        NotEnoughFreeSpace,
        ServerError,
        UserCancel
    }
}
