﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UpdateEngine
{
    internal class HashWorker
    {
        public event CheckingProgressChangedEventHandler CheckingProgressChanged;

        private const char Delimiter = ';';

        public async Task<List<FileDetail>> ComputeFromDirectoryAsync(CancellationToken ct, string sourcePath)
        {
            if (!Directory.Exists(sourcePath))
            {
                throw new IOException("Source directory does not exist: " + sourcePath);
            }

            var fileDelails = new List<FileDetail>();

            string[] fileNames = Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories);
            for (int i = 0; i < fileNames.Length; i++)
            {
                if (ct.IsCancellationRequested)
                {
                    throw new OperationCanceledException();
                }
                else
                {
                    string fullFileName = fileNames[i];
                    string shortFileName = Path.GetFileName(fullFileName);
                    bool isException = UpdaterManager.FileExceptions.Any(f => String.Compare(f, shortFileName, true) == 0);

                    if (isException)
                        continue;

                    if (UpdaterManager.UpdateFileName == shortFileName)
                        continue;

                    OnCheckingProgressChanged(new CheckingProgressChangedEventArgs { Name = shortFileName });

                    await Task.Factory.StartNew(new Action(() =>
                    {
                        using (var stream = File.OpenRead(fullFileName))
                        {
                            string path = UpdaterManager.GetRelativeDirectoryPath(fullFileName, sourcePath);
                            string hash = HashUtility.GetHashChecksum(stream);
                            long size = stream.Length;

                            fileDelails.Add(new FileDetail { Path = path, Hash = hash, Size = size });
                        }
                    }));
                }
            }

            return fileDelails;
        }

        public List<FileDetail> LoadFromFile(string filePath, string fileName)
        {
            string[] data = File.ReadAllLines(Path.Combine(filePath, fileName));
            var fileDetails = new List<FileDetail>();
            foreach (string str in data)
            {
                string[] strData = str.Split(Delimiter);
                string path = strData[0];
                string hash = strData[1];
                long size = long.Parse(strData[2]);

                fileDetails.Add(new FileDetail { Path = path, Hash = hash, Size = size });
            }

            return fileDetails;
        }

        public async Task WriteToFileAsync(List<FileDetail> fileDetails, string filePath, string fileName)
        {
            string path = Path.Combine(filePath, fileName);
            var text = new StringBuilder();
            foreach (FileDetail fileDetail in fileDetails)
            {
                text.AppendFormat("{1}{0}{2}{0}{3}", Delimiter, fileDetail.Path, fileDetail.Hash, fileDetail.Size);
                text.AppendLine();
            }

            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                byte[] data = UnicodeEncoding.Default.GetBytes(text.ToString());
                await fileStream.WriteAsync(data, 0, data.Length);
            }
        }

        private void OnCheckingProgressChanged(CheckingProgressChangedEventArgs args)
        {
            if (CheckingProgressChanged != null)
                CheckingProgressChanged(null, args);
        }
    }
}
