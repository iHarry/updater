﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace UpdateEngine
{
    public static class HashUtility
    {
        public static string GetHashChecksum(Stream stream)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                byte[] checkSum = md5.ComputeHash(stream);

                string checkSumStr = Convert.ToBase64String(checkSum);

                return checkSumStr;
            }
        }
    }
}
