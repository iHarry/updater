﻿using System;

namespace UpdateEngine
{
    public delegate void DownloadProgressChangedEventHandler(object sender, DownloadProgressChangedEventArgs args);

    public class DownloadProgressChangedEventArgs : EventArgs
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public long DownloadedSize { get; set; }
    }
}
